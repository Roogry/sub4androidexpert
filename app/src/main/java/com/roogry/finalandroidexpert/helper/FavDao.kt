package com.roogry.finalandroidexpert.helper

import android.database.Cursor
import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy.REPLACE
import androidx.room.Query
import com.roogry.finalandroidexpert.model.FavMovie
import com.roogry.finalandroidexpert.model.FavTvShow

@Dao
interface FavDao {

    @Query("SELECT * FROM ${FavMovie.TABLE_NAME}")
    fun getAllFavMovies(): List<FavMovie>

    @Query("SELECT * FROM ${FavMovie.TABLE_NAME}")
    fun getAllFavMoviesCursor(): Cursor

    @Query("SELECT * FROM ${FavMovie.TABLE_NAME} WHERE ${FavMovie.COLUMN_ID} =:id")
    fun getMovieByID(id: Long): Cursor

    @Query("SELECT * FROM ${FavMovie.TABLE_NAME} WHERE ${FavMovie.COLUMN_ID} =:id")
    fun getIsFavMovie(id: Int?): List<FavMovie>

    @Insert(onConflict = REPLACE)
    fun setFavMovie(movies: FavMovie)

    @Insert(onConflict = REPLACE)
    fun setFavMovieCursor(movies: FavMovie): Long

    @Insert
    fun insertAll(cheeses: Array<FavMovie?>): LongArray

    @Delete
    fun delFavMovie(movies: FavMovie)

    @Query("SELECT * FROM tvShows")
    fun getAllFavTvShows(): List<FavTvShow>

    @Query("SELECT * FROM tvShows WHERE id =:id")
    fun getIsFavTvShow(id: Int?): List<FavTvShow>

    @Insert(onConflict = REPLACE)
    fun setFavTvShow(tvShows: FavTvShow)

    @Delete
    fun delFavTvShow(tvShows: FavTvShow)

}