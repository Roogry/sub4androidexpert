package com.roogry.finalandroidexpert.helper

import android.app.AlarmManager
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.media.RingtoneManager
import android.os.Build
import android.util.Log
import android.widget.Toast
import androidx.core.app.NotificationCompat
import androidx.core.content.ContextCompat
import com.loopj.android.http.JsonHttpResponseHandler
import com.loopj.android.http.RequestParams
import com.roogry.finalandroidexpert.R
import com.roogry.finalandroidexpert.model.MovieItem
import cz.msebera.android.httpclient.Header
import org.json.JSONException
import org.json.JSONObject
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class ReminderMovie : BroadcastReceiver() {
    private val extraType = "type"

    private val dailyID = 101
    private val releaseID = 102

    override fun onReceive(context: Context, intent: Intent) {
        val type = intent.getStringExtra(extraType)

        if (type == typeDaily) {
            val dailyTitle = context.resources.getString(R.string.daily_message_title)
            val dailyMessage = context.resources.getString(R.string.daily_message)

            showToast(context, dailyTitle, dailyMessage)
            showAlarmNotification(context, dailyTitle, dailyMessage, dailyID)
        } else {
            val requestParams = RequestParams()
            WebReq[context, getDateNow(), requestParams, this.ResponseHandler(context)]
        }
    }

    private fun showAlarmNotification(context: Context, title: String, message: String, notifId: Int) {
        val channelId = "Channel_1"
        val channelName = "Movie Reminder"

        val notificationManagerCompat = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        val alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
        val builder = NotificationCompat.Builder(context, channelId)
            .setSmallIcon(R.drawable.ic_local_movies_black_24dp)
            .setContentTitle(title)
            .setContentText(message)
            .setColor(ContextCompat.getColor(context, android.R.color.transparent))
            .setVibrate(longArrayOf(1000, 1000, 1000, 1000, 1000))
            .setSound(alarmSound)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channel = NotificationChannel(
                channelId,
                channelName,
                NotificationManager.IMPORTANCE_DEFAULT
            )

            channel.enableVibration(true)
            channel.vibrationPattern = longArrayOf(1000, 1000, 1000, 1000, 1000)

            builder.setChannelId(channelId)
            notificationManagerCompat.createNotificationChannel(channel)
        }

        val notification = builder.build()
        notificationManagerCompat.notify(notifId, notification)
    }

    fun setReminder(context: Context?, type: String) {
        val alarmManager = context?.getSystemService(Context.ALARM_SERVICE) as AlarmManager
        val intent = Intent(context, ReminderMovie::class.java)
        intent.putExtra(extraType, type)

        val calendar = Calendar.getInstance()

        if (type == typeDaily) {
            calendar.set(Calendar.HOUR_OF_DAY, 7)
            calendar.set(Calendar.MINUTE, 0)
            calendar.set(Calendar.SECOND, 0)
        } else {
            calendar.set(Calendar.HOUR_OF_DAY, 8)
            calendar.set(Calendar.MINUTE, 0)
            calendar.set(Calendar.SECOND, 0)
        }

        val requestCode = if (type.equals(typeDaily, ignoreCase = true)) dailyID else releaseID
        val pendingIntent = PendingIntent.getBroadcast(context, requestCode, intent, 0)
        alarmManager.setInexactRepeating(
            AlarmManager.RTC_WAKEUP,
            calendar.timeInMillis,
            AlarmManager.INTERVAL_DAY,
            pendingIntent
        )

        Toast.makeText(context, "$type turn on", Toast.LENGTH_SHORT).show()
    }

    fun cancelReminder(context: Context?, type: String) {
        val alarmManager = context?.getSystemService(Context.ALARM_SERVICE) as AlarmManager
        val intent = Intent(context, ReminderMovie::class.java)
        val requestCode = if (type.equals(typeDaily, ignoreCase = true)) dailyID else releaseID
        val pendingIntent = PendingIntent.getBroadcast(context, requestCode, intent, 0)
        pendingIntent.cancel()

        alarmManager.cancel(pendingIntent)

        Toast.makeText(context, "$type turn off", Toast.LENGTH_SHORT).show()
    }

    fun isAlarmSet(context: Context?, type: String): Boolean {
        val intent = Intent(context, ReminderMovie::class.java)
        val requestCode = if (type.equals(typeDaily, ignoreCase = true)) dailyID else releaseID

        return PendingIntent.getBroadcast(context, requestCode, intent, PendingIntent.FLAG_NO_CREATE) != null
    }

    private fun showToast(context: Context, title: String, message: String) {
        Toast.makeText(context, "$title : $message", Toast.LENGTH_LONG).show()
    }

    private fun getDateNow(): String {
        val dateFormatter = SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH)
        dateFormatter.isLenient = false
        val today = Date()
        return dateFormatter.format(today)
    }

    companion object {
        const val typeDaily = "Daily reminder"
        const val typeRelease = "Release today reminder"
    }

    inner class ResponseHandler(var context: Context) : JsonHttpResponseHandler() {
        var movies: ArrayList<MovieItem> = ArrayList()

        override fun onFailure(
            statusCode: Int,
            headers: Array<out Header>?,
            responseString: String?,
            throwable: Throwable?
        ) {
            super.onFailure(statusCode, headers, responseString, throwable)
            Log.e("error get release", throwable?.message)
        }

        override fun onSuccess(statusCode: Int, headers: Array<out Header>?, response: JSONObject?) {
            super.onSuccess(statusCode, headers, response)
            movies.clear()
            Log.d("response ", response.toString())

            try {
                val moviesJSON = response?.getJSONArray("results")
                if (moviesJSON != null){
                    if (moviesJSON.length() > 0) {

                        for (i in 0 until moviesJSON.length()) {
                            val movie = moviesJSON.getJSONObject(i)
                            showAlarmNotification(
                                context,
                                movie.getString("title"),
                                "${movie.getString("title")} has been release today!",
                                releaseID + movie.getInt("id")
                            )
                        }
                    }
                }
            } catch (e: JSONException) {
                e.printStackTrace()
                Log.e("error trycatch", e.message)
            }
        }
    }
}