package com.roogry.finalandroidexpert.helper

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.roogry.finalandroidexpert.model.FavMovie
import com.roogry.finalandroidexpert.model.FavTvShow

@Database(entities = [FavMovie::class, FavTvShow::class], version = 1)
abstract class HelperDatabase : RoomDatabase() {

    abstract fun favDao(): FavDao

    companion object {
        private var INSTANCE: HelperDatabase? = null

        fun getInstance(context: Context): HelperDatabase? {
            if (INSTANCE == null) {
                synchronized(HelperDatabase::class) {
                    INSTANCE = Room.databaseBuilder(
                        context.applicationContext,
                        HelperDatabase::class.java, "favorite.db"
                    )
                        .build()
                }
            }
            return INSTANCE
        }

        fun destroyInstance() {
            INSTANCE = null
        }
    }
}