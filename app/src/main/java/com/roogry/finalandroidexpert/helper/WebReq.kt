package com.roogry.finalandroidexpert.helper

import android.content.Context
import android.util.Log
import com.loopj.android.http.AsyncHttpClient
import com.loopj.android.http.RequestParams
import com.loopj.android.http.ResponseHandlerInterface
import com.roogry.finalandroidexpert.BuildConfig

class WebReq {

    companion object {
        var client = AsyncHttpClient(true, 80, 443)

        operator fun get(
            context: Context,
            date: String,
            params: RequestParams,
            responseHandler: ResponseHandlerInterface
        ) {
            client.get(context, getAbsoluteUrl(date), params, responseHandler)
        }

        private fun getAbsoluteUrl(date: String): String {
            Log.d(
                "response URL: ",
                BuildConfig.API_URL + "discover/movie?api_key=" + BuildConfig.API_KEY + "&primary_release_date.gte=" + date + "&primary_release_date.lte=" + date
            )
            return BuildConfig.API_URL + "discover/movie?api_key=" + BuildConfig.API_KEY + "&primary_release_date.gte=" + date + "&primary_release_date.lte=" + date
        }
    }

    operator fun get(context: Context, url: String, params: RequestParams, responseHandler: ResponseHandlerInterface) {
        client.get(context, getAbsoluteUrl(url), params, responseHandler)
    }


}