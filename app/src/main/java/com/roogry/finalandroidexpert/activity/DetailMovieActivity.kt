package com.roogry.finalandroidexpert.activity

import android.os.Bundle
import android.text.method.ScrollingMovementMethod
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.bumptech.glide.Glide
import com.roogry.finalandroidexpert.R
import com.roogry.finalandroidexpert.R.id.add_favorite
import com.roogry.finalandroidexpert.R.menu.fav_menu
import com.roogry.finalandroidexpert.helper.HelperDatabase
import com.roogry.finalandroidexpert.model.FavMovie
import com.roogry.finalandroidexpert.model.MovieItem
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_detail_movie.*

class DetailMovieActivity : AppCompatActivity() {

    private lateinit var movieItem: MovieItem
    private lateinit var favMovie: FavMovie
    private var menuItem: Menu? = null

    private var isFavorite: Boolean = false

    private var helperDatabase: HelperDatabase? = null
    private val compositeDisposable = CompositeDisposable()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_movie)
        initLayout()

        helperDatabase = HelperDatabase.getInstance(this)
        movieItem = intent.getParcelableExtra(EXTRA)
        favMovie = FavMovie(
            movieItem.overview,
            movieItem.title,
            movieItem.posterPath,
            movieItem.backdropPath,
            movieItem.voteAverage,
            movieItem.id
        )

        setData()
    }

    private fun favoriteState(id: Int?) {
        compositeDisposable.add(Observable.fromCallable { helperDatabase?.favDao()?.getIsFavMovie(id) }
            .subscribeOn(Schedulers.computation())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                setFavorite(it?.isNotEmpty()!!)
            })
    }

    private fun setFavorite(isFavorite: Boolean) {
        if (isFavorite) {
            menuItem!!.getItem(0)?.icon = ContextCompat.getDrawable(this, R.drawable.ic_favorite_black_24dp)
            this.isFavorite = true
        } else {
            menuItem!!.getItem(0)?.icon = ContextCompat.getDrawable(this, R.drawable.ic_favorite_border_black_24dp)
            this.isFavorite = false
        }
    }

    private fun setData() {
        txtTitle.text = movieItem.title
        txtRating.text = movieItem.voteAverage.toString()
        txtOverview.text = movieItem.overview
        Glide.with(this)
            .load("https://image.tmdb.org/t/p/w185${movieItem.posterPath}")
            .into(mvPoster)

        Glide.with(this)
            .load("https://image.tmdb.org/t/p/w780${movieItem.backdropPath}")
            .into(mvBackdrop)
    }

    private fun initLayout() {
        supportActionBar?.title = "Detail Movie"
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        txtOverview.movementMethod = ScrollingMovementMethod()
    }

    override fun onDestroy() {
        super.onDestroy()
        HelperDatabase.destroyInstance()
        compositeDisposable.dispose()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(fav_menu, menu)
        menuItem = menu

        favoriteState(movieItem.id)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            add_favorite -> {
                if (isFavorite) removeFromFavorite() else addToFavorite()
                true
            }

            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun addToFavorite() {
        compositeDisposable.add(Observable.fromCallable { helperDatabase?.favDao()?.setFavMovie(favMovie) }
            .subscribeOn(Schedulers.computation())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe())
        isFavorite = true
        setFavorite(true)
    }

    private fun removeFromFavorite() {
        compositeDisposable.add(Observable.fromCallable { helperDatabase?.favDao()?.delFavMovie(favMovie) }
            .subscribeOn(Schedulers.computation())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe())
        isFavorite = false
        setFavorite(false)
    }

    override fun onSupportNavigateUp(): Boolean {
        this.finish()
        return true
    }

    companion object {
        const val EXTRA: String = "EXTRA"
    }
}
