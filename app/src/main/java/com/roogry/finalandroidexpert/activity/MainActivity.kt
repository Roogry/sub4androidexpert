package com.roogry.finalandroidexpert.activity

import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.roogry.finalandroidexpert.R
import com.roogry.finalandroidexpert.fragment.FavoriteFragment
import com.roogry.finalandroidexpert.fragment.MovieFragment
import com.roogry.finalandroidexpert.fragment.TvShowFragment


class MainActivity : AppCompatActivity() {

    private val onNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        val fragment: Fragment

        when (item.itemId) {
            R.id.nav_movies -> {
                fragment = MovieFragment.newIntance()
                supportFragmentManager.beginTransaction()
                    .replace(R.id.content, fragment)
                    .commit()
                return@OnNavigationItemSelectedListener true
            }
            R.id.nav_tv_shows -> {
                fragment = TvShowFragment.newIntance()
                supportFragmentManager.beginTransaction()
                    .replace(R.id.content, fragment)
                    .commit()
                return@OnNavigationItemSelectedListener true
            }
            R.id.nav_favorite -> {
                fragment = FavoriteFragment.newIntance()
                supportFragmentManager.beginTransaction()
                    .replace(R.id.content, fragment)
                    .commit()
                return@OnNavigationItemSelectedListener true
            }
        }
        false
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val navView: BottomNavigationView = findViewById(R.id.nav_view)
        navView.setOnNavigationItemSelectedListener(onNavigationItemSelectedListener)

        if (savedInstanceState == null) {
            navView.selectedItemId = R.id.nav_movies
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.main_menu, menu)

        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_change_settings -> {
                val intent = Intent(this, SettingsActivity::class.java)
                startActivity(intent)
            }
        }
        return super.onOptionsItemSelected(item)
    }

    fun setActionBarTitle(title: String) {
        supportActionBar?.title = title
    }
}
