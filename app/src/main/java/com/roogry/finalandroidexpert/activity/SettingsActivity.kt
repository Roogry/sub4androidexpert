package com.roogry.finalandroidexpert.activity

import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.provider.Settings
import androidx.appcompat.app.AppCompatActivity
import androidx.preference.Preference
import androidx.preference.PreferenceFragmentCompat
import androidx.preference.SwitchPreferenceCompat
import com.roogry.finalandroidexpert.R
import com.roogry.finalandroidexpert.helper.ReminderMovie

class SettingsActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.settings_activity)

        supportFragmentManager
            .beginTransaction()
            .replace(
                R.id.settings,
                SettingsFragment()
            )
            .commit()
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

    }

    class SettingsFragment : PreferenceFragmentCompat(), SharedPreferences.OnSharedPreferenceChangeListener {
        private lateinit var releaseKey: String
        private lateinit var dailyKey: String
        private lateinit var releasePref: SwitchPreferenceCompat
        private lateinit var dailyPref: SwitchPreferenceCompat

        private var reminderMovie: ReminderMovie = ReminderMovie()

        override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
            setPreferencesFromResource(R.xml.root_preferences, rootKey)
            init()
            setLastSetting()
        }

        private fun setReminder(type: String) {
            reminderMovie.setReminder(this.context, type)
        }

        private fun cancelReminder(type: String) {
            reminderMovie.cancelReminder(this.context, type)
        }

        private fun setLastSetting() {
            dailyPref.isChecked = reminderMovie.isAlarmSet(this.context, ReminderMovie.typeDaily)
            releasePref.isChecked = reminderMovie.isAlarmSet(this.context, ReminderMovie.typeRelease)
        }

        private fun init() {
            releaseKey = resources.getString(R.string.release)
            dailyKey = resources.getString(R.string.daily)
            releasePref = findPreference(releaseKey)!!
            dailyPref = findPreference(dailyKey)!!
        }

        override fun onResume() {
            super.onResume()
            preferenceScreen.sharedPreferences.registerOnSharedPreferenceChangeListener(this)
        }

        override fun onPause() {
            super.onPause()
            preferenceScreen.sharedPreferences.unregisterOnSharedPreferenceChangeListener(this)
        }

        override fun onSharedPreferenceChanged(sharedPreferences: SharedPreferences, key: String) {
            if (key == releaseKey) {
                val isChecked = sharedPreferences.getBoolean(releaseKey, false)
                releasePref.isChecked = isChecked

                if (isChecked) setReminder(ReminderMovie.typeRelease) else cancelReminder(ReminderMovie.typeRelease)
            }
            if (key == dailyKey) {
                val isChecked = sharedPreferences.getBoolean(dailyKey, false)
                dailyPref.isChecked = isChecked

                if (isChecked) setReminder(ReminderMovie.typeDaily) else cancelReminder(ReminderMovie.typeDaily)
            }
        }

        override fun onPreferenceTreeClick(preference: Preference): Boolean {
            return when (preference.key) {
                getString(R.string.language) -> {
                    startActivity(Intent(Settings.ACTION_LOCALE_SETTINGS))
                    true
                }
                else -> {
                    super.onPreferenceTreeClick(preference)
                }
            }
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        this.finish()
        return true
    }
}