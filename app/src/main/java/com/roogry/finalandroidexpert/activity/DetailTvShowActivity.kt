package com.roogry.finalandroidexpert.activity

import android.os.Bundle
import android.text.method.ScrollingMovementMethod
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.bumptech.glide.Glide
import com.roogry.finalandroidexpert.R
import com.roogry.finalandroidexpert.helper.HelperDatabase
import com.roogry.finalandroidexpert.model.FavTvShow
import com.roogry.finalandroidexpert.model.TvShowItem
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_detail_tv_show.*

class DetailTvShowActivity : AppCompatActivity() {

    private lateinit var tvShowItem: TvShowItem
    private lateinit var favTvShow: FavTvShow
    private var menuItem: Menu? = null

    private var isFavorite: Boolean = false

    private var helperDatabase: HelperDatabase? = null
    private val compositeDisposable = CompositeDisposable()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_tv_show)
        initLayout()

        helperDatabase = HelperDatabase.getInstance(this)
        tvShowItem = intent.getParcelableExtra(EXTRA)
        favTvShow = FavTvShow(
            tvShowItem.overview,
            tvShowItem.name,
            tvShowItem.posterPath,
            tvShowItem.backdropPath,
            tvShowItem.voteAverage,
            tvShowItem.id
        )

        setData()
    }

    private fun favoriteState(id: Int?) {
        compositeDisposable.add(Observable.fromCallable { helperDatabase?.favDao()?.getIsFavTvShow(id) }
            .subscribeOn(Schedulers.computation())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                setFavorite(it?.isNotEmpty())
            })
    }

    private fun setFavorite(isFavorite: Boolean? = false) {
        if (isFavorite!!) {
            menuItem?.getItem(0)?.icon = ContextCompat.getDrawable(this, R.drawable.ic_favorite_black_24dp)
            this.isFavorite = true
        } else {
            menuItem?.getItem(0)?.icon = ContextCompat.getDrawable(this, R.drawable.ic_favorite_border_black_24dp)
            this.isFavorite = false
        }
    }

    private fun setData() {
        txtTitle.text = tvShowItem.name
        txtRating.text = tvShowItem.voteAverage.toString()
        txtOverview.text = tvShowItem.overview
        Glide.with(this)
            .load("https://image.tmdb.org/t/p/w185${tvShowItem.posterPath}")
            .into(mvPoster)

        Glide.with(this)
            .load("https://image.tmdb.org/t/p/w780${tvShowItem.backdropPath}")
            .into(mvBackdrop)
    }

    private fun initLayout() {
        supportActionBar?.title = "Detail TvShow"
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        txtOverview.movementMethod = ScrollingMovementMethod()
    }

    override fun onDestroy() {
        super.onDestroy()
        HelperDatabase.destroyInstance()
        compositeDisposable.dispose()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.fav_menu, menu)
        this.menuItem = menu

        favoriteState(tvShowItem.id)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.add_favorite -> {
                if (isFavorite) removeFromFavorite() else addToFavorite()
                true
            }

            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun addToFavorite() {
        compositeDisposable.add(Observable.fromCallable { helperDatabase?.favDao()?.setFavTvShow(favTvShow) }
            .subscribeOn(Schedulers.computation())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe())
        setFavorite(true)
    }

    private fun removeFromFavorite() {
        compositeDisposable.add(Observable.fromCallable { helperDatabase?.favDao()?.delFavTvShow(favTvShow) }
            .subscribeOn(Schedulers.computation())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe())
        setFavorite(false)
    }

    override fun onSupportNavigateUp(): Boolean {
        this.finish()
        return true
    }

    companion object {
        const val EXTRA: String = "EXTRA"
    }
}
