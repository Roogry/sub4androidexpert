package com.roogry.finalandroidexpert.viewModel

import android.content.Context
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.roogry.finalandroidexpert.helper.FavDao
import com.roogry.finalandroidexpert.helper.HelperDatabase
import com.roogry.finalandroidexpert.model.FavMovie
import com.roogry.finalandroidexpert.model.FavTvShow
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

class FavoriteViewModel : ViewModel() {
    private var movies = MutableLiveData<ArrayList<FavMovie>>()
    private var tvShows = MutableLiveData<ArrayList<FavTvShow>>()

    private var favDao: FavDao? = null
    private val compositeDisposable = CompositeDisposable()

    fun getMovies(): MutableLiveData<ArrayList<FavMovie>> {
        return movies
    }

    fun setMovies(context: Context) {
        val movies: ArrayList<FavMovie> = arrayListOf()
        favDao = HelperDatabase.getInstance(context)?.favDao()
        compositeDisposable.add(Observable.fromCallable { favDao?.getAllFavMovies() }
            .subscribeOn(Schedulers.computation())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                if(it != null){
                    movies.clear()
                    movies.addAll(it)
                }
            })
        this.movies.postValue(movies)
    }

    fun getTvShows(): MutableLiveData<ArrayList<FavTvShow>> {
        return tvShows
    }

    fun setTvShows(context: Context) {
        val tvShows: ArrayList<FavTvShow> = arrayListOf()
        favDao = HelperDatabase.getInstance(context)?.favDao()
        compositeDisposable.add(Observable.fromCallable { favDao?.getAllFavTvShows() }
            .subscribeOn(Schedulers.computation())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                if(it != null){
                    tvShows.clear()
                    tvShows.addAll(it)
                }
            })
        this.tvShows.postValue(tvShows)
    }
}