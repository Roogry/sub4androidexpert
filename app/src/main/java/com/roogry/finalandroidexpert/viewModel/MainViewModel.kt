package com.roogry.finalandroidexpert.viewModel

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.roogry.finalandroidexpert.BuildConfig
import com.roogry.finalandroidexpert.api.RetrofitClient
import com.roogry.finalandroidexpert.api.UserService
import com.roogry.finalandroidexpert.model.MovieItem
import com.roogry.finalandroidexpert.model.MovieResponse
import com.roogry.finalandroidexpert.model.TvShowItem
import com.roogry.finalandroidexpert.model.TvShowResponse
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MainViewModel : ViewModel() {
    private var movies = MutableLiveData<ArrayList<MovieItem>>()
    private var tvShows = MutableLiveData<ArrayList<TvShowItem>>()
    private var userService: UserService = RetrofitClient.service

    fun getMovies(): MutableLiveData<ArrayList<MovieItem>> {
        return movies
    }

    fun setMovies() {
        userService = RetrofitClient.service
        userService.getMovies(BuildConfig.API_KEY).enqueue(object : Callback<MovieResponse> {
            override fun onFailure(call: Call<MovieResponse>, t: Throwable) {
                Log.e("Failure retrofit", t.message)
            }

            override fun onResponse(call: Call<MovieResponse>, response: Response<MovieResponse>) {
                movies.postValue(response.body()?.results)
            }
        })
    }

    fun setTvShows() {
        userService = RetrofitClient.service
        userService.getTvShows(BuildConfig.API_KEY).enqueue(object : Callback<TvShowResponse> {
            override fun onFailure(call: Call<TvShowResponse>, t: Throwable) {
                Log.e("Failure retrofit", t.message)
            }

            override fun onResponse(call: Call<TvShowResponse>, response: Response<TvShowResponse>) {
                tvShows.postValue(response.body()?.results)
            }
        })
    }

    fun getTvShows(): MutableLiveData<ArrayList<TvShowItem>> {
        return tvShows
    }

    fun searchMovies(query: String) {
        userService = RetrofitClient.service
        userService.searchMovie(BuildConfig.API_KEY, query).enqueue(object : Callback<MovieResponse> {
            override fun onFailure(call: Call<MovieResponse>, t: Throwable) {
                Log.e("Failure retrofit", t.message)
            }

            override fun onResponse(call: Call<MovieResponse>, response: Response<MovieResponse>) {
                movies.postValue(response.body()?.results)
            }
        })
    }

    fun searchTvShows(query: String) {
        userService = RetrofitClient.service
        userService.searchTvShows(BuildConfig.API_KEY, query).enqueue(object : Callback<TvShowResponse> {
            override fun onFailure(call: Call<TvShowResponse>, t: Throwable) {
                Log.e("Failure retrofit", t.message)
            }

            override fun onResponse(call: Call<TvShowResponse>, response: Response<TvShowResponse>) {
                tvShows.postValue(response.body()?.results)
            }
        })
    }
}