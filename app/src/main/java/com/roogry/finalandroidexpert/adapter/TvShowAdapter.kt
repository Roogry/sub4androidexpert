package com.roogry.finalandroidexpert.adapter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.roogry.finalandroidexpert.R
import com.roogry.finalandroidexpert.activity.DetailTvShowActivity
import com.roogry.finalandroidexpert.model.TvShowItem
import kotlinx.android.synthetic.main.movie_item.view.*

class TvShowAdapter(private val context: Context?, private val tvShows: ArrayList<TvShowItem>) :
    RecyclerView.Adapter<TvShowAdapter.Holder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {
        return Holder(LayoutInflater.from(parent.context).inflate(R.layout.movie_item, parent, false))
    }

    override fun getItemCount(): Int = tvShows.size

    override fun onBindViewHolder(holder: Holder, position: Int) {
        holder.bind(context, tvShows[position])
    }

    class Holder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(context: Context?, tvShow: TvShowItem) {
            val sRatting =
                context?.resources?.getString(context.resources.getIdentifier("rating", "string", context.packageName))
            val rating = StringBuilder().append(sRatting).append(tvShow.voteAverage.toString())
            itemView.txtTitle.text = tvShow.name
            itemView.txtRating.text = rating
            itemView.txtOverview.text = tvShow.overview

            Glide.with(context!!)
                .load("https://image.tmdb.org/t/p/w185${tvShow.posterPath}")
                .into(itemView.mvPoster)

            itemView.setOnClickListener {
                val intent = Intent(context, DetailTvShowActivity::class.java)
                intent.putExtra(DetailTvShowActivity.EXTRA, tvShow)
                context.startActivity(intent)
            }
        }
    }
}