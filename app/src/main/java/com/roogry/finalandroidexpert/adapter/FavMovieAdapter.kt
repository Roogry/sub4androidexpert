package com.roogry.finalandroidexpert.adapter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.roogry.finalandroidexpert.R
import com.roogry.finalandroidexpert.activity.DetailMovieActivity
import com.roogry.finalandroidexpert.model.FavMovie
import com.roogry.finalandroidexpert.model.MovieItem
import kotlinx.android.synthetic.main.movie_item.view.*


class FavMovieAdapter(private val context: Context, private val favMovie: ArrayList<FavMovie>) :
    RecyclerView.Adapter<FavMovieAdapter.Holder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {
        return Holder(LayoutInflater.from(parent.context).inflate(R.layout.movie_item, parent, false))
    }

    override fun getItemCount(): Int = favMovie.size

    override fun onBindViewHolder(holder: Holder, position: Int) {
        holder.bind(context, favMovie[position])
    }

    class Holder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(context: Context, favMovie: FavMovie) {

            val sRatting =
                context.resources.getString(context.resources.getIdentifier("rating", "string", context.packageName))
            val rating = StringBuilder().append(sRatting).append(favMovie.voteAverage.toString())
            itemView.txtTitle.text = favMovie.title
            itemView.txtRating.text = rating
            itemView.txtOverview.text = favMovie.overview

            Glide.with(context)
                .load("https://image.tmdb.org/t/p/w185${favMovie.posterPath}")
                .into(itemView.mvPoster)

            itemView.setOnClickListener {
                val intent = Intent(context, DetailMovieActivity::class.java)
                intent.putExtra(DetailMovieActivity.EXTRA, getMovie(favMovie))
                context.startActivity(intent)
            }
        }

        private fun getMovie(favMovie: FavMovie): MovieItem? {
            return MovieItem(
                favMovie.overview,
                favMovie.title,
                favMovie.posterPath,
                favMovie.backdropPath,
                favMovie.voteAverage,
                favMovie.id
            )
        }
    }
}