package com.roogry.finalandroidexpert.fragment

import android.os.Bundle
import android.view.*
import androidx.appcompat.widget.SearchView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.roogry.finalandroidexpert.R
import com.roogry.finalandroidexpert.activity.MainActivity
import com.roogry.finalandroidexpert.adapter.TvShowAdapter
import com.roogry.finalandroidexpert.model.TvShowItem
import com.roogry.finalandroidexpert.viewModel.MainViewModel
import kotlinx.android.synthetic.main.fragment_tv_show.*
import kotlinx.android.synthetic.main.fragment_tv_show.view.*
import java.util.*

class TvShowFragment : Fragment() {

    private lateinit var adapter: TvShowAdapter
    private lateinit var viewModel: MainViewModel
    private var keyword: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        (activity as MainActivity).setActionBarTitle(resources.getString(R.string.tv_shows))
        setHasOptionsMenu(true)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_tv_show, container, false)

        viewModel = ViewModelProviders.of(this).get(MainViewModel::class.java)
        viewModel.getTvShows().observe(this, getTvShow)

        loadData(keyword)

        view.swipe.setOnRefreshListener {
            loadData(keyword)
        }

        return view
    }

    private fun loadData(keyword: String) {
        if (keyword == "") {
            viewModel.setTvShows()
        } else {
            viewModel.searchTvShows(keyword)
        }
    }

    private val getTvShow =
        Observer<ArrayList<TvShowItem>> { tvShows ->
            if (tvShows != null) {
                swipe.isRefreshing = true
                adapter = TvShowAdapter(context, tvShows)
                rv.adapter = adapter
                rv.layoutManager = LinearLayoutManager(context)
                swipe.isRefreshing = false
            }
        }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)

        inflater.inflate(R.menu.search_menu, menu)
    }

    override fun onPrepareOptionsMenu(menu: Menu) {
        super.onPrepareOptionsMenu(menu)

        val mSearchAction = menu.findItem(R.id.action_search)
        val searchView = mSearchAction?.actionView as SearchView
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String): Boolean {
                if (query.isNotEmpty()) {
                    keyword = query
                    loadData(keyword)
                } else {
                    keyword = ""
                    loadData(keyword)
                }
                return true
            }

            override fun onQueryTextChange(newText: String): Boolean {
                if (newText.isNotEmpty()) {
                    keyword = newText
                    loadData(keyword)
                } else {
                    keyword = ""
                    loadData(keyword)
                }
                return false
            }
        })
    }

    companion object {
        fun newIntance(): TvShowFragment = TvShowFragment()
    }
}
