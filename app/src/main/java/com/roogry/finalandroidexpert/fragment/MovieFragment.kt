package com.roogry.finalandroidexpert.fragment

import android.os.Bundle
import android.view.*
import androidx.appcompat.widget.SearchView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.roogry.finalandroidexpert.R
import com.roogry.finalandroidexpert.activity.MainActivity
import com.roogry.finalandroidexpert.adapter.MovieAdapter
import com.roogry.finalandroidexpert.model.MovieItem
import com.roogry.finalandroidexpert.viewModel.MainViewModel
import kotlinx.android.synthetic.main.fragment_movie.*
import kotlinx.android.synthetic.main.fragment_movie.view.*
import java.util.*

class MovieFragment : Fragment() {

    private lateinit var adapter: MovieAdapter
    private lateinit var viewModel: MainViewModel
    private var keyword: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        (activity as MainActivity).setActionBarTitle(resources.getString(R.string.movies))
        setHasOptionsMenu(true)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_movie, container, false)

        viewModel = ViewModelProviders.of(this).get(MainViewModel::class.java)
        viewModel.getMovies().observe(this, getMovie)

        loadData(keyword)

        view.swipe.setOnRefreshListener {
            loadData(keyword)
        }

        return view
    }

    private fun loadData(keyword: String) {
        if (keyword == "") {
            viewModel.setMovies()
        } else {
            viewModel.searchMovies(keyword)
        }
    }

    private val getMovie =
        Observer<ArrayList<MovieItem>> { movies ->
            if (movies != null) {
                swipe.isRefreshing = true
                adapter = MovieAdapter(context, movies)
                rv.adapter = adapter
                rv.layoutManager = LinearLayoutManager(context)
                swipe.isRefreshing = false
            }
        }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)

        inflater.inflate(R.menu.search_menu, menu)
    }

    override fun onPrepareOptionsMenu(menu: Menu) {
        super.onPrepareOptionsMenu(menu)
        val mSearchAction = menu.findItem(R.id.action_search)
        val searchView = mSearchAction?.actionView as SearchView
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String): Boolean {
                if (query.isNotEmpty()) {
                    keyword = query
                    loadData(keyword)
                } else {
                    keyword = ""
                    loadData(keyword)
                }
                return true
            }

            override fun onQueryTextChange(newText: String): Boolean {
                if (newText.isNotEmpty()) {
                    keyword = newText
                    loadData(keyword)
                } else {
                    keyword = ""
                    loadData(keyword)
                }
                return false
            }
        })
    }

    companion object {
        fun newIntance(): MovieFragment = MovieFragment()
        private val LOADER_MOVIE = 1
    }
}
