package com.roogry.finalandroidexpert.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.roogry.finalandroidexpert.R
import com.roogry.finalandroidexpert.activity.MainActivity
import com.roogry.finalandroidexpert.adapter.TabAdapter
import com.roogry.finalandroidexpert.fragment.favorite.FavMovieFragment
import com.roogry.finalandroidexpert.fragment.favorite.FavTvFragment
import kotlinx.android.synthetic.main.fragment_favorite.view.*

class FavoriteFragment : Fragment() {

    private lateinit var tabAdapter: TabAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        (activity as MainActivity).setActionBarTitle(resources.getString(R.string.favorite))
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_favorite, container, false)

        tabAdapter = TabAdapter(fragmentManager)
        tabAdapter.addFragment(FavMovieFragment(), context!!.resources.getString(R.string.movies))
        tabAdapter.addFragment(FavTvFragment(), context!!.resources.getString(R.string.tv_shows))

        view.viewPager.adapter = tabAdapter
        view.tabLayout.setupWithViewPager(view.viewPager)

        return view
    }

    companion object {
        fun newIntance(): FavoriteFragment = FavoriteFragment()
    }

}
