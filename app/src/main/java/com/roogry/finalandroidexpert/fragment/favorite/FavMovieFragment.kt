package com.roogry.finalandroidexpert.fragment.favorite

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.roogry.finalandroidexpert.R
import com.roogry.finalandroidexpert.adapter.FavMovieAdapter
import com.roogry.finalandroidexpert.model.FavMovie
import com.roogry.finalandroidexpert.viewModel.FavoriteViewModel
import kotlinx.android.synthetic.main.fragment_fav_movie.view.*
import kotlinx.android.synthetic.main.fragment_movie.*
import java.util.*

class FavMovieFragment : Fragment() {

    private lateinit var adapter: FavMovieAdapter
    private lateinit var viewModel: FavoriteViewModel
    private lateinit var swipe: SwipeRefreshLayout

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        viewModel = ViewModelProviders.of(this).get(FavoriteViewModel::class.java)
        viewModel.getMovies().observe(this, getMovie)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_fav_movie, container, false)
        swipe = view.swipe

        loadData()

        swipe.setOnRefreshListener {
            loadData()
        }
        return view
    }

    override fun onResume() {
        super.onResume()
        loadData()
    }

    private val getMovie =
        Observer<ArrayList<FavMovie>> { movies ->
            if (movies != null) {
                swipe.isRefreshing = true
                adapter = FavMovieAdapter(context!!, movies)
                rv.adapter = adapter
                rv.layoutManager = LinearLayoutManager(context)
                swipe.isRefreshing = false
            }
        }

    private fun loadData() {
        viewModel.setMovies(context!!)
        swipe.isRefreshing = true
    }


}
