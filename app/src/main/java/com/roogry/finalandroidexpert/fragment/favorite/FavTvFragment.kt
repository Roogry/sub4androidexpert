package com.roogry.finalandroidexpert.fragment.favorite

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.roogry.finalandroidexpert.R
import com.roogry.finalandroidexpert.adapter.FavTvShowAdapter
import com.roogry.finalandroidexpert.model.FavTvShow
import com.roogry.finalandroidexpert.viewModel.FavoriteViewModel
import kotlinx.android.synthetic.main.fragment_fav_tv.*
import kotlinx.android.synthetic.main.fragment_fav_tv.view.*
import java.util.*

class FavTvFragment : Fragment() {

    private lateinit var adapter: FavTvShowAdapter
    private lateinit var viewModel: FavoriteViewModel
    private lateinit var swipe: SwipeRefreshLayout

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        viewModel = ViewModelProviders.of(this).get(FavoriteViewModel::class.java)
        viewModel.getTvShows().observe(this, getTvShow)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_fav_tv, container, false)
        swipe = view.swipe

        loadData()

        swipe.setOnRefreshListener {
            loadData()
        }
        return view
    }

    override fun onResume() {
        super.onResume()
        loadData()
    }

    private val getTvShow =
        Observer<ArrayList<FavTvShow>> { tvShows ->
            if (tvShows != null) {
                swipe.isRefreshing = true
                adapter = FavTvShowAdapter(context!!, tvShows)
                rv.adapter = adapter
                rv.layoutManager = LinearLayoutManager(context)
                swipe.isRefreshing = false
            }
        }

    private fun loadData() {
        viewModel.setTvShows(context!!)
        swipe.isRefreshing = true
    }

}
