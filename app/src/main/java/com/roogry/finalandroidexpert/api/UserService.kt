package com.roogry.finalandroidexpert.api

import com.roogry.finalandroidexpert.model.MovieResponse
import com.roogry.finalandroidexpert.model.TvShowResponse
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface UserService {
    @GET("discover/movie?language=en-US")
    fun getMovies(@Query("api_key") API_KEY: String): Call<MovieResponse>

    @GET("discover/tv?language=en-US")
    fun getTvShows(@Query("api_key") API_KEY: String): Call<TvShowResponse>

    @GET("search/tv?language=en-US")
    fun searchTvShows(
        @Query("api_key") API_KEY: String,
        @Query("query") query: String
    ): Call<TvShowResponse>

    @GET("search/movie?language=en-US")
    fun searchMovie(
        @Query("api_key") API_KEY: String,
        @Query("query") query: String
    ): Call<MovieResponse>
}