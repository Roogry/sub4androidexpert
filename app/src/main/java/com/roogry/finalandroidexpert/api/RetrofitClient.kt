package com.roogry.finalandroidexpert.api

import com.roogry.finalandroidexpert.BuildConfig
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


internal object RetrofitClient {

    private val retrofit = Retrofit.Builder()
        .baseUrl(BuildConfig.API_URL)
        .addConverterFactory(GsonConverterFactory.create())
        .build()
    val service = retrofit.create(UserService::class.java)!!
}