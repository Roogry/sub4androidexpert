package com.roogry.finalandroidexpert.widget

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.os.Binder
import android.os.Bundle
import android.widget.RemoteViews
import android.widget.RemoteViewsService
import com.bumptech.glide.Glide
import com.roogry.finalandroidexpert.R
import com.roogry.finalandroidexpert.helper.MappingHelper.Companion.mapCursorToArrayList
import com.roogry.finalandroidexpert.model.FavMovie
import com.roogry.finalandroidexpert.provider.FavMovieContentProvider

class StackRemoteViewsFactory(var context: Context) : RemoteViewsService.RemoteViewsFactory {

    private var movies: ArrayList<FavMovie> = ArrayList()

    override fun onCreate() {
        initData()
    }

    @SuppressLint("Recycle")
    private fun initData() {
        movies.clear()
        val movieCursor = context.contentResolver.query(
            FavMovieContentProvider.URI_MOVIE,
            null,
            null,
            null,
            null
        )

        if (movieCursor != null){
            movies.addAll(mapCursorToArrayList(movieCursor))
            movieCursor.close()
        }
    }

    override fun onDataSetChanged() {
        val token = Binder.clearCallingIdentity()
        initData()
        Binder.restoreCallingIdentity(token)
    }

    override fun onDestroy() {}

    override fun getCount(): Int = movies.size


    override fun getViewAt(position: Int): RemoteViews {
        val movie = movies[position]
        val rv = RemoteViews(context.packageName, R.layout.widget_item)

        val bitmapUrl = Glide.with(context)
            .asBitmap()
            .load("https://image.tmdb.org/t/p/w342${movie.posterPath}")
            .submit()
            .get()

        rv.setImageViewBitmap(R.id.imageView, bitmapUrl)
        rv.setTextViewText(R.id.txtTitle, movie.title)

        val extras = Bundle()
        extras.putInt(MovieFavoriteWidget.EXTRA_ITEM, position)
        val fillInIntent = Intent()
        fillInIntent.putExtras(extras)

        rv.setOnClickFillInIntent(R.id.imageView, fillInIntent)
        return rv
    }

    override fun getLoadingView(): RemoteViews? {
        return null
    }

    override fun getViewTypeCount(): Int {
        return 1
    }

    override fun getItemId(i: Int): Long {
        return if (movies.isNotEmpty()) movies[0].id!!.toLong() else i.toLong()
    }

    override fun hasStableIds(): Boolean {
        return false
    }
}