package com.roogry.finalandroidexpert.model

import android.os.Parcelable
import android.provider.BaseColumns
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.android.parcel.Parcelize

@Parcelize
@Entity(tableName = FavMovie.TABLE_NAME)
data class FavMovie(

    @ColumnInfo(name = "overview")
    var overview: String? = null,

    @ColumnInfo(name = "title")
    var title: String? = null,

    @ColumnInfo(name = "poster_path")
    var posterPath: String? = null,

    @ColumnInfo(name = "backdrop_path")
    var backdropPath: String? = null,

    @ColumnInfo(name = "vote_average")
    var voteAverage: Double? = null,

    @PrimaryKey(autoGenerate = false)
    @ColumnInfo(name = COLUMN_ID)
    var id: Int? = null
) : Parcelable {

    companion object {
        const val TABLE_NAME = "movies"
        const val COLUMN_ID = BaseColumns._ID
        const val COLUMN_TITLE = "title"
        const val COLUMN_posterPath = "poster_path"
        const val COLUMN_backdropPath = "backdrop_path"
        const val COLUMN_voteAverage = "vote_average"
        const val COLUMN_overview = "overview"
    }

}