package com.roogry.finalandroidexpert.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize


@Parcelize
data class MovieItem(
    @field:SerializedName("overview")
    var overview: String? = null,

    @field:SerializedName("title")
    var title: String? = null,

    @field:SerializedName("poster_path")
    var posterPath: String? = null,

    @field:SerializedName("backdrop_path")
    var backdropPath: String? = null,

    @field:SerializedName("vote_average")
    var voteAverage: Double? = null,

    @field:SerializedName("id")
    var id: Int? = null
) : Parcelable {
    /*fun Note(cursor: Cursor) {
        this.id = cursor.getInt(cursor.getColumnIndex(_ID))
        this.title = cursor.getString(cursor.getColumnIndex(MovieColumns.TITLE))
        this.overview = cursor.getString(cursor.getColumnIndex(MovieColumns.DESCRIPTION))
        this.voteAverage = cursor.getDouble(cursor.getColumnIndex(MovieColumns.DATE))
        this.posterPath = cursor.getString(cursor.getColumnIndex(MovieColumns.DATE))
        this.backdropPath = cursor.getString(cursor.getColumnIndex(MovieColumns.DATE))
    }*/
}
