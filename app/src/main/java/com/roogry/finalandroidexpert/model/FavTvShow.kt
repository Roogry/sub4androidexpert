package com.roogry.finalandroidexpert.model

import android.os.Parcelable
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.android.parcel.Parcelize

@Parcelize
@Entity(tableName = "tvShows")
data class FavTvShow(

    @ColumnInfo(name = "overview")
    var overview: String? = null,

    @ColumnInfo(name = "name")
    var name: String? = null,

    @ColumnInfo(name = "poster_path")
    var posterPath: String? = null,

    @ColumnInfo(name = "backdrop_path")
    var backdropPath: String? = null,

    @ColumnInfo(name = "vote_average")
    var voteAverage: Double? = null,

    @PrimaryKey(autoGenerate = false) @ColumnInfo(name = "id")
    var id: Int? = null
) : Parcelable