package com.roogry.finalandroidexpert.provider;

import android.content.*;
import android.database.Cursor;
import android.net.Uri;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.roogry.finalandroidexpert.helper.FavDao;
import com.roogry.finalandroidexpert.helper.HelperDatabase;
import com.roogry.finalandroidexpert.model.FavMovie;

public class FavMovieContentProvider extends ContentProvider {

    private static final UriMatcher matcher = new UriMatcher(UriMatcher.NO_MATCH);
    private static final int CODE_MOVIE_DIR = 1;
    private static final int CODE_MOVIE_ITEM = 2;

    private static final String AUTHORITY = "com.roogry.finalandroidexpert";
    public static final Uri URI_MOVIE = new Uri.Builder().scheme("content")
            .authority(AUTHORITY)
            .appendPath(FavMovie.TABLE_NAME)
            .build();

    static {
        matcher.addURI(AUTHORITY, FavMovie.TABLE_NAME, CODE_MOVIE_DIR);
        matcher.addURI(AUTHORITY, FavMovie.TABLE_NAME + "/*", CODE_MOVIE_ITEM);
    }

    @Override
    public boolean onCreate() {
        return false;
    }

    @Nullable
    @Override
    public Cursor query(@NonNull final Uri uri, @Nullable String[] projection, @Nullable String selection, @Nullable String[] selectionArgs, @Nullable String sortOrder) {
        final int code = matcher.match(uri);

        if (code == CODE_MOVIE_DIR || code == CODE_MOVIE_ITEM) {
            final Context context = getContext();
            if (context == null) {
                return null;
            }

            final FavDao favDao = HelperDatabase.Companion.getInstance(context).favDao();
            final Cursor[] cursor = new Cursor[1];

            Thread getMovie = new Thread(new Runnable() {
                @Override
                public void run() {
                    if (code == CODE_MOVIE_DIR) {
                        cursor[0] = favDao.getAllFavMoviesCursor();
                    } else {
                        cursor[0] = favDao.getMovieByID(ContentUris.parseId(uri));
                    }
                }
            });

            getMovie.start();
            try {
                getMovie.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            cursor[0].setNotificationUri(context.getContentResolver(), uri);
            return cursor[0];
        } else {
            throw new IllegalArgumentException("Unknown URI : " + uri);
        }
    }

    @Nullable
    @Override
    public String getType(@NonNull Uri uri) {
        switch (matcher.match(uri)) {
            case CODE_MOVIE_DIR:
                return "com.roogry.finalandroidexpert/" + AUTHORITY + "." + FavMovie.TABLE_NAME;
            case CODE_MOVIE_ITEM:
                return "com.roogry.finalandroidexpert/" + AUTHORITY + "." + FavMovie.TABLE_NAME;
            default:
                throw new IllegalArgumentException("Unknown URI : " + uri);
        }
    }

    @Nullable
    @Override
    public Uri insert(@NonNull Uri uri, @Nullable ContentValues values) {
        return null;
    }

    @Override
    public int delete(@NonNull Uri uri, @Nullable String selection, @Nullable String[] selectionArgs) {
        return 0;
    }

    @Override
    public int update(@NonNull Uri uri, @Nullable ContentValues values, @Nullable String selection, @Nullable String[] selectionArgs) {
        return 0;
    }
}
